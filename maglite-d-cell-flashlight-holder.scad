include <libopenscad/mtube.scad>;
include <libopenscad/mcylinder.scad>;
include <libopenscad/pegboard-rail.scad>;

$fn = 180; 

rev = 2;

arc = 240;
switch_cutout_diameter = 30;
switch_cutout_depth_ratio = 1/4;
head_diameter = 57; // diameter of fattest part of head
head_column_length = 37; // length of the cylindrical portion of head

body_diameter = 41; // includes +1mm for protruding switch


h = base_size * 2;
cuff_wall_thickness = 25.4 / 4;
id = body_diameter + 8;
od = id + (cuff_wall_thickness * 4);


module cuff(h = 10, od = 50, id = 40, align = [0, 0, 0], chamfer_ratio = 1/4, angle = 270, revision_pips = 0) {
    
    difference() {
        union() {
            // cuff
            mtube(h = h, od = od, id = id, align = [0, 0, 0], chamfer = ((od - id) / 2) * chamfer_ratio, angle = angle);
            color("blue") {
                for(rot = [-1, 1]) {
                    rotate([0, 0, rot * (angle / 2)]) {
                        translate([(od/2) - ((od - id) / 4), 0, 0]) {
                            mcylinder(h = h, d = (od - id) / 2, chamfer = ((od - id) / 2) * chamfer_ratio);
                        }
                    }
                }
            }
        }
        
        
        union() {
            // pips
            pipsize = (od - id) * chamfer_ratio * 1/2;
            color("fuchsia")
            rotate([0, 0, -angle / 2])
            for(a = [1 : revision_pips]) {
                rotate([0, 0, a * pipsize * 4])
                translate([(id / 2) + (od - id) / 4, 0, h/2]) {
                    echo("a:", a);
                    mcylinder(d = pipsize, h = pipsize, align = [0, 0, 0]);
                }
            }
        }
    }
}

module slug() {
    

    color("lightblue") {
        union() {
            // head
            sphere(d = head_diameter);
            translate([0, 0, -1]) {
                mcylinder(h = head_column_length + 2, d = head_diameter, chamfer = 1, align = [0, 0, 1]); 
            }
        }
    }
    
    color("pink") {
        
        union() {
            // body
            rotate([0, 0, 180]) {
                difference() {
                    mcylinder(h = 245 + 2, d = body_diameter, chamfer = 1, align = [0, 0, -1]);
                    translate([(body_diameter / 2) + (switch_cutout_diameter * switch_cutout_depth_ratio), 0, -((head_diameter / 2) + 20)]) {
                        // switch cutout
                        sphere(d = switch_cutout_diameter);
                    }
                }
                translate([(body_diameter / 2) - (switch_cutout_diameter / 2) + 1, 0, -((head_diameter / 2) + 20)]) {
                    // switch button
                    sphere(d = switch_cutout_diameter);
                }
            }        
        }
    }    
}


copies = 2;

intersection() {
    union() {
union(){
color("lightblue") {
    translate([0, 0, -h/2])
    rail(height = 2, chamfer = plate_thickness / 4, copies = copies);
}
color("orange") {
    translate([0, od/2, 0]) {
        rotate([0, 0, -90])
        cuff(h = h, od = od, id = id, angle = arc, revision_pips = rev);
    }
}
 
    translate([0, 1, 0]) {
        difference() {
        color("pink") mcube([base_size * copies, base_size, h], align = [0, 1, 0], chamfer = plate_thickness / 4); 
        translate([0, (od - id) / 4, -1]) {
            color("red")
            mcylinder(h = h + 3, d = od - (od - id) / 2, align = [0,1,0]);
        }
    }
    
}
}
}
mcube([od * 2, od * 2, h - 0.02], align = [0, 0, 0]);
}

translate([0, 0, head_diameter]) {
//    slug();
}